<?php
require_once 'vendor/tpl.php';
require_once 'Request.php';
require_once 'ContactList.php';
require_once 'Contact.php';

$request = new Request($_REQUEST);


$command = $request->param('command')
    ? $request->param('command')
    : 'show_list_page';

if ($command == 'show_list_page') {
    $allContacts = getContacts();
    foreach ($allContacts as $contact) {
        $contact->phones = join(', ', $contact->phones);
    }
    $data = [
            "template" => "list.html",
        "contacts" => $allContacts,
    ];

    print(renderTemplate("html/list.html", $data));
} elseif ($command == "add") {
    $contactToAddName = $_POST["firstName"];
    $contactToAddSurname = $_POST["lastName"];
    $contactToAddPhone1 = $_POST["phone1"];
    $contactToAddPhone2 = $_POST["phone2"];
    $contactToAddPhone3 = $_POST["phone3"];
    $contactToAdd = new Contact($contactToAddName, $contactToAddSurname);
    if ($contactToAddPhone1) {
        $contactToAdd->phones[] = $contactToAddPhone1;
    }
    if ($contactToAddPhone2) {
        $contactToAdd->phones[] = $contactToAddPhone2;
    }
    if ($contactToAddPhone3) {
        $contactToAdd->phones[] = $contactToAddPhone3;
    }
    addContact($contactToAdd);
    header('Location: ?command=added');
} elseif ($command == 'show_add_page') {

    $data = [
        "template" => "add.html"
    ];

    print(renderTemplate("html/add.html", $data));
} else if ($command == "added") {

    $allContacts = getContacts();
    foreach ($allContacts as $contact) {
        $contact->phones = join(', ', $contact->phones);
    }
    $data = [
        "template" => "list.html",
        "contacts" => $allContacts,
    ];

    print(renderTemplate("html/list.html", $data));
}
