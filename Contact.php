<?php
class Contact {
    public $name;
    public $surname;
    public $phones;

    public function __construct($name = "", $surname = "", $phones = []) {
        $this->name = $name;
        $this->surname = $surname;
        $this->phones = $phones;
    }

    public function __toString() {
        return "Contact{name: $this->name, surname: $this->surname, phone: $this->phones}";
    }
}
