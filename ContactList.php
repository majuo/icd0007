<?php

require_once 'Contact.php';

const USERNAME = "majuo";
const PASSWORD = "CF99";


function addContact(Contact $contactToAdd) {
    $address = 'mysql:host=db.mkalmo.xyz;dbname=majuo';
    $connection = new PDO($address, USERNAME, PASSWORD,
        [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);

    $contacts = getContacts();

    $itemExists = false;
    foreach ($contacts as $contact) {
        if ($contact->name === $contactToAdd->name && $contact->surname === $contactToAdd->surname){
            $itemExists = true;
            break;
        }
    }

    if ($itemExists == false) {
        $stmt = $connection->prepare(
            'insert into contacts (first_name, last_name) values (:name, :surname)');
        $stmt->bindValue(':name', $contactToAdd->name);
        $stmt->bindValue(':surname', $contactToAdd->surname);
        $stmt->execute();
    }

    $stmt = $connection->prepare(
        'select * from contacts where contacts.first_name =:name 
                         and contacts.last_name = :surname');
    $stmt->bindValue(':name', $contactToAdd->name);
    $stmt->bindValue(':surname', $contactToAdd->surname);
    $stmt->execute();
    $id = 0;
    foreach ($stmt as $row) {
        $id = $row['id'];
    }

    $stmt = $connection->prepare(
        'insert into phones (phone, contact_id) values (:phone, :contact_id)');
    $stmt->bindValue('contact_id', $id);
    foreach ($contactToAdd->phones as $phone) {
        $stmt->bindValue(':phone', $phone);
        $statement = $connection->prepare(
            'select * from phones where phone = :phone and contact_id = :id');
        $statement->bindValue(":phone", $phone);
        $statement->bindValue(':id', $id);
        $statement->execute();
        if ($statement->rowCount() == 0) {
            $stmt->execute();
        }
    }
}

function getContacts() {
    $address = 'mysql:host=db.mkalmo.xyz;dbname=majuo';
    $connection = new PDO($address, USERNAME, PASSWORD,
        [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);

    $stmt = $connection->prepare(
        'select * from contacts, phones where contacts.id = phones.contact_id');
    $stmt->execute();
    $contacts = [];

    foreach ($stmt as $row) {
        $containsContact = false;
        foreach ($contacts as $contact) {
            if ($contact->name === $row['first_name'] && $contact->surname === $row["last_name"]) {
                $contact->phones[] = $row["phone"];
                $containsContact = true;
                break;
            }
        }
        if (!$containsContact) {
            $contactToAdd = new Contact($row["first_name"], $row["last_name"]);
            $contactToAdd->phones[] = $row["phone"];
            $contacts[] = $contactToAdd;
        }
    }

    return $contacts;
}
